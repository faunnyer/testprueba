import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'testPrueba'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    expect(component.title).toEqual('testPrueba');
  });

  it('should render title', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('testPrueba app is running!');
  });

  it('should return the correct sum for non overlapping intervals', function () {
    var test1 = [[1, 5]];
    var test2 = [[1, 5], [6, 10]];
    expect(component.sumarIntervalor(test1)).toEqual(4);
    expect(component.sumarIntervalor(test2)).toEqual(8);
  });

  it('should return the correct sum for overlapping intervals', function () {
    var test1 = [[1, 5], [1, 5]];
    var test2 = [[1, 4], [7, 10], [3, 5]];
    expect(component.sumarIntervalor(test1)).toEqual(4);
    expect(component.sumarIntervalor(test2)).toEqual(7);
  });

  it('Correcta suma del Intervalo 3', function () {
    const inter = [[1, 2], [6, 10], [11, 15]];
    const inter2 = [[1,5], [10, 20], [1, 6], [16, 19], [5, 11]];
    const suma = component.sumarIntervalor(inter);
    const suma2 = component.sumarIntervalor(inter2);
    expect(suma).toEqual(9);
    expect(suma2).toEqual(19);
  });

  it('verificar que se envie un array definido', function () {
    const inter = [[1, 10],[6, 10],[12, 20]];
    const inter2 = [[1, 10],[6, 8]];
    expect(component.sumarIntervalor(inter)).toBeDefined();
    expect(component.sumarIntervalor(inter2)).toBeDefined();
  });

  it('verificar que se envie una array con data', function () {
    const inter = [[1, 10]];
    const inter2 = [[5, 20]];
    const suma = component.sumarIntervalor(inter);
    const suma2 = component.sumarIntervalor(inter2);
    expect(inter.length).toEqual(1);
    expect(inter2.length).toEqual(1);
  });

  it('verificar que se envie una array sin data', function () {
    const inter = [];
    component.sumarIntervalor(inter);
    expect(inter.length).toEqual(0);
  });

  it('verificar mensaje Lista vacía si no se envian datos', function () {
    const inter = [];
    component.sumarIntervalor(inter);
    expect(component.mensaje).toEqual('Lista vacía');
  });

  it('verificar mensaje exito si se envian datos', function () {
    const inter = [[1, 4]];
    component.sumarIntervalor(inter);
    expect(component.mensaje).toEqual('Éxito');
  });

});
