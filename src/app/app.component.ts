import { Component } from '@angular/core';
// export type miTipo = Number[][];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'testPrueba';
  mensaje = '';

  // inter: Array<Array<Number>> = [[1, 4], [3, 5], [7, 10]];
  inter: Number[][] = [[1, 4], [3, 5], [7, 10]];

  public sumarIntervalor(inter) {
    if (inter.length > 0) {
      const numeroIntervalos = new Set();
      inter.forEach(([inicio, fin]) => {
        for (let i = inicio; i < fin; i++) numeroIntervalos.add(i);
      });
      this.mensaje = 'Éxito';
      return numeroIntervalos.size;
    } else {
      this.mensaje = 'Lista vacía';
    }
  }





}
